package strategy;

import strategy.acao.OperacaoMatematica;

/**
 * 
 * Classe respons�vel por receber a interface em tempo de execu��o, e efetuar o c�lculo dependendo da implementa��o que receber
 * 
 * @author Gabriel Falc�o
 *
 */
public class Calculadora {

	public int calcular(int primeiraVariavel, int segundaVariavel, OperacaoMatematica operacao) {
		return operacao.calcular(primeiraVariavel, segundaVariavel);
	}
}
