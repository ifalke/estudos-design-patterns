package strategy;

import strategy.acao.impl.Adicao;
import strategy.acao.impl.Divisao;
import strategy.acao.impl.Multiplicacao;
import strategy.acao.impl.Subtracao;

/**
 * 
 * Classe simulando o cliente, que apenas chamar� a classe de contexto e executar�.
 * 
 * @author Gabriel Falc�o
 *
 */
public class Client {

	public static void main(String[] args) {
	
		Calculadora calculadora = new Calculadora();
		
		System.out.println("Exemplo de Strategy");
		System.out.println("Ao receber a classe respons�vel pela adi��o:");
		System.out.println(calculadora.calcular(10, 10, new Adicao()));
		System.out.println("\nAo receber a classe respons�vel pela subtra��o:");
		System.out.println(calculadora.calcular(10, 10, new Subtracao()));
		System.out.println("\nAo receber a classe respons�vel pela divis�o:");
		System.out.println(calculadora.calcular(10, 10, new Divisao()));
		System.out.println("\nAo receber a classe respons�vel pela multiplica��o:");
		System.out.println(calculadora.calcular(10, 10, new Multiplicacao()));
	}

}
