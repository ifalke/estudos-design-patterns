package strategy.acao;

public interface OperacaoMatematica {
	public int calcular(int primeiraVariavel, int segundaVariavel);
}
