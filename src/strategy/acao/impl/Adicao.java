package strategy.acao.impl;

import strategy.acao.OperacaoMatematica;

public class Adicao implements OperacaoMatematica {

	@Override
	public int calcular(int primeiraVariavel, int segundaVariavel) {
		return primeiraVariavel + segundaVariavel;
	}

}
