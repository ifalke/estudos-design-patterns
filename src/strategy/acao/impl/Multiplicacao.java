package strategy.acao.impl;

import strategy.acao.OperacaoMatematica;

public class Multiplicacao implements OperacaoMatematica {

	@Override
	public int calcular(int primeiraVariavel, int segundaVariavel) {
		return primeiraVariavel * segundaVariavel;
	}

}
